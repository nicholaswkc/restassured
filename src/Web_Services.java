import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

import static io.restassured.module.jsv.JsonSchemaValidator.*;


public class Web_Services {

	public Web_Services() {
	}
	
	@BeforeClass
	public void initPath() {
		
		RestAssured.baseURI = "http://localhost:9876";
	}
	
	@Test
	public void checkResponseCodeForCorrectRequest() {
				
		given().
		when().
			get("/api/f1/2016/drivers.json").
		then().
			assertThat().
			statusCode(200);
	}
	
	@DataProvider(name = "circuits")
	public String[][] createCircuitData() {
		return new String[][] {
				{ "monza", "Italy" },
				{ "spa", "Belgium" },
				{ "sepang", "Malaysia" }
		};
	}
	
	@Test(dataProvider = "circuits")
	public void checkCountryForCircuit(String circuitName, String circuitCountry) {
		
		given().
			pathParam("circuitName", circuitName).
		when().
			get("/api/f1/circuits/{circuitName}.json").
		then().
			assertThat().
			body("MRData.CircuitTable.Circuits.Location[0].country",equalTo(circuitCountry));
	}
	
	@Test
	public void testGetSingleUserAsXml() {
	  expect().
	    statusCode(200).
	    body(
	      "user.email", equalTo("test@hascode.com"),
	      "user.firstName", equalTo("Tim"),
	      "user.lastName", equalTo("Testerman"),
	      "user.id", equalTo("1")).
	    when().
	    get("/service/single-user/xml");
	}
	
	/*
	 * http://www.hascode.com/2011/10/testing-restful-web-services-made-easy-using-the-rest-assured-framework/
	 * 
	 * 
	 */
	
	

}
